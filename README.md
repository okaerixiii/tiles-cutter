# Tiles Cutter

**Copyright © 2018 Sergey Artemov, All rights reserved. License: GPLv3.0+**
    
Cut tile atlas to single tiles

### Usage:

* Select tile atlas from your disk (press "Open..." button)
* Enter width and height of single tile and press "OK"
* Join tiles to bigger with pressed left mose button
* Press right mouse button for cutting
* Results placed to 'pieces' directory
