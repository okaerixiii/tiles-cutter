#!/usr/bin/env python3

'''
    Copyright © 2018, All rights reserved
    Author: Sergey Artemov
    License: GPLv3.0+
'''

import os

import tkinter as tk
import tkinter.filedialog as dialog
from tkinter import messagebox

import pygame
from pygame.locals import *

class TkWindow():
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('Options')
    
        self.file_path = tk.Entry(self.root)
        self.open_btn = tk.Button(self.root, text='Open...', command=self.__open_file)
        self.wid_label = tk.Label(self.root, text='Width: ')
        self.wid = tk.Entry(self.root)
        self.hei_label = tk.Label(self.root, text='Height: ')
        self.hei = tk.Entry(self.root)
        self.ok_btn = tk.Button(self.root, text='OK', command=self.__ok)

        self.file_path.insert(0, 'atlas.png')
        self.wid.insert(0, '32')
        self.hei.insert(0, '32')

        self.file_path.grid(row=0, column=0, columnspan=3, sticky='nsew')
        self.open_btn.grid(row=0, column=3, sticky='nsew')
        self.wid_label.grid(row=1, column=0, sticky='nsew')
        self.wid.grid(row=1, column=1, sticky='nsew')
        self.hei_label.grid(row=1, column=2, sticky='nsew')
        self.hei.grid(row=1, column=3, sticky='nsew')
        self.ok_btn.grid(row=2, column=1, columnspan=2, sticky='nsew')

    def update(self):
        self.root.update()

    def destroy(self):
        self.root.destroy()

    def __open_file(self):
        self.file_path.delete(0, tk.END)
        self.file_path.insert(0, dialog.askopenfilenames())

    def __ok(self):
        is_ok = True
        
        path = self.file_path.get()

        if not os.path.exists(path):
            messagebox.showerror('Error', 'Path [%s] not exists' % path)
            is_ok = False
            
        if is_ok and not path.endswith(('.png', '.gif', '.jpg')):
            messagebox.showerror('Error', 'Unsupported file format. Must be PNG, GIF or JPG')
            is_ok = False
        
        try:
            width = int(self.wid.get())
        except:
            messagebox.showerror('Error', 'Width must be integer value')
            is_ok = False
            
        try:
            height = int(self.hei.get())
        except:
            messagebox.showerror('Error', 'Height must be integer value')
            is_ok = False
        
        if is_ok:
            PyGameWindow(path, width, height)

class PyGameWindow():
    def __init__(self, path, width, height):
        pygame.init()
        
        self.out_directory = 'pieces'
        
        self.path = path
        self.width = width
        self.height = height
        
        self.win = pygame.display.set_mode((100, 100))
        pygame.display.set_caption('Progress')

        self.atlas = pygame.image.load(path).convert_alpha()
        self.atl_size = self.atlas.get_size()
        
        self.win = pygame.display.set_mode(self.atl_size)

        self.clock = pygame.time.Clock()

        self.start()

        pygame.quit()

    def progress(self, percents):
        width = 0
        step = 218 / 100
        if percents > 0:
            width = int(step * percents)
        elif percents == 100:
            width = 218
            
        self.win.fill((255, 255, 255))

        pygame.draw.rect(self.win, (0, 0, 0), [10, 10, 230, 30], 1)
        pygame.draw.rect(self.win, (0, 0, 0), [15, 15, 220, 20], 1)
        pygame.draw.rect(self.win, (255, 90, 90), [16, 16, width, 18])
        
    def handle_events(self, events):
        for event in events:
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                self.done = True
                
    def init_rects(self):
        w = int(self.atl_size[0] / self.width)
        h = int(self.atl_size[1] / self.height)
        
        self.rects = []
        self.selection = None
        
        for y in range(h):
            for x in range(w):
                self.rects.append(pygame.Rect(x * self.width,
                                              y * self.height,
                                              self.width,
                                              self.height))
                
    def draw_rects(self):        
        for rect in self.rects:
            pygame.draw.rect(self.win, (90, 90, 90), rect, 1)
            
    def draw_selection(self):
        w = self.end_point[0] - self.start_point[0]
        h = self.end_point[1] - self.start_point[1]
        
        self.selection = pygame.Rect(self.start_point, (w, h))
        
        pygame.draw.rect(self.win, (255, 0, 0), self.selection, 1)
        
    def reorganize_rects(self):
        list = self.selection.collidelistall(self.rects)
        
        if len(list) <= 1:
            return
        
        rect_union = []
        
        for i in list:
            rect_union.append(self.rects[i])
        
        self.rects[list[0]].unionall_ip(rect_union)
        
        list.pop(0)
        list.sort(reverse=True)
        
        for i in list:
            self.rects.pop(i)
            
    def cutting(self):
        
        count = 0
        
        if not os.path.exists(self.out_directory):
            os.mkdir(self.out_directory)
        
        for rect in self.rects:
            tile = self.atlas.subsurface(rect)
            pygame.image.save(tile, os.path.join(self.out_directory, '%05i.png' % (count)))
            count += 1
    
    def start(self):
        self.done = False
        
        self.is_mouse1_pressed = False
        
        self.start_point = (0, 0)
        self.end_point = (0, 0)
    
        self.init_rects()
        
        while not self.done:
            self.clock.tick(20)
            
            self.win.fill((0, 0, 0))
            
            self.handle_events(pygame.event.get())
            
            buttons = pygame.mouse.get_pressed()
            position = pygame.mouse.get_pos()
            
            self.win.blit(self.atlas, (0, 0))
            self.draw_rects()
            
            if buttons[0] == 1 and not self.is_mouse1_pressed:
                self.start_point = position
                self.end_point = position
                self.is_mouse1_pressed = True
            elif buttons[0] == 1 and self.is_mouse1_pressed:
                self.end_point = position
                self.draw_selection()
            else:
                if self.selection:
                    self.reorganize_rects()
                self.start_point = (0, 0)
                self.end_point = (0, 0)
                self.is_mouse1_pressed = False
                
            if buttons[2] == 1:
                self.cutting()
                
                
            pygame.display.flip()
                     
